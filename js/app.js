$(function() {
    $(document).foundation();

    var $pdfOrbit = $('#pdf-orbit');
    if ($pdfOrbit.length !== 0) {
        new Foundation.PdfOrbit($pdfOrbit);
    }
})
