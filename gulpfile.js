const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const pump = require('pump');

const Fiber = require('fibers');
const sass = require('gulp-sass');
sass.compiler = require('sass');

const sassOptions = {
    fiber: Fiber,
    outputStyle: 'compressed'
};

function pdf_orbit(cb) {
    pump([
        gulp.src('./node_modules/foundation-pdf-orbit/dist/**/*'),
        gulp.dest('./js-compiled')
    ], cb);
}

function scss(cb) {
    pump([
        gulp.src('./scss/**/*.scss'),
        sourcemaps.init(),
        sass(sassOptions).on('error', sass.logError),
        rename({ suffix: '.min' }),
        sourcemaps.write(''),
        gulp.dest('./css-compiled')
    ], cb);
}

exports.default = gulp.parallel(pdf_orbit, scss);
