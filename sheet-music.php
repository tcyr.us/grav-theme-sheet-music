<?php
namespace Grav\Theme;

use Grav\Common\Theme;

class SheetMusic extends Theme
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onThemeInitialized' => ['onThemeInitialized', 0]
        ];
    }

    public function onThemeInitialized()
    {
        if ($this->isAdmin()) {
            $this->active = false;
            return;
        }

        $this->enable([
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0]
        ]);
    }

    public function onTwigSiteVariables()
    {
        $assets = $this->grav['assets'];
        $assets->registerCollection('jquery3', [
        	'https://unpkg.com/jquery@3.4.1/dist/jquery.min.js'
        ]);
        $assets->registerCollection('foundation-css', [
            'https://unpkg.com/foundation-sites@6.6.1/dist/css/foundation.min.css'
        ]);
        $assets->registerCollection('foundation-icons', [
            'https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css'
        ]);
        $assets->registerCollection('foundation-js', [
            'https://unpkg.com/foundation-sites@6.6.1/dist/js/foundation.min.js'
        ]);
        $assets->registerCollection('pdfjs', [
			'https://unpkg.com/pdfjs-dist@2.3.200/build/pdf.min.js'
        ]);
        $assets->registerCollection('what-input', [
            'https://unpkg.com/what-input@5.2.7/dist/what-input.min.js',
        ]);
    }
}
